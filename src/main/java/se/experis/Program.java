package se.experis;

import org.json.JSONArray;
import se.experis.APIRequest;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        inputMenu();

    }
    /*
    * Take in the user input and send the information to APIRequest class
    * The program will always print out the characters in the books
    */
    public static void inputMenu() {
        APIRequest character = new APIRequest();
        Scanner sc = new Scanner(System.in);
        System.out.println("Search after a GoT character by entering a id");
        String id;
        while (!sc.hasNextLine()) {
            System.out.println("enter wrong input");
            id = sc.nextLine();
            inputMenu();
        }
        id = sc.nextLine();
        System.out.println("Do you wanna see the sworn member for this character? print y. Otherwise print no");
        String option = sc.nextLine();
        character.getCharacter(id, option);
        character.getBooks();
        makeAnotherSearchMenu();
    }
    /*
    * Give the user option to search after another character
    */
    public static void makeAnotherSearchMenu() {
        System.out.println("Do you wanna do another search press 1" +"\n" +
                            "You wanna exit the program press 2 ");
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();
        if(option == 1) {
            inputMenu();
        }
        if(option == 2) {
            System.exit(1);
        }
    }
}
