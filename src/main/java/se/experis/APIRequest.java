package se.experis;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

public class APIRequest {

    // Contructor
    public APIRequest() {
    }
    /*
    * Function will take in the user input and will print out the information
    */
    public void getCharacter(String characterNumber,String option) {
        // Hold the character information as a JSON object
        JSONObject currentCharacter = getCharacterInformationByHttpRequest(characterNumber);
        StringBuilder printOutInformation = new StringBuilder();
        printOutInformation.append("Name: "  +currentCharacter.getString("name") + "\n");
        printOutInformation.append("Gender " + currentCharacter.getString("gender") + "\n");
        printOutInformation.append("Titles: " + "\n");

        // loop to receive the titles from the JSON Array and add it to the StringBuilder
        for(Object title: currentCharacter.getJSONArray("titles")) {
            printOutInformation.append("\t" +title + "\n");
        }
        // loop to receive the aliases from the JSON Array and add it to the StringBuilder
        printOutInformation.append("Aliases: " + "\n");
        for(Object alias:currentCharacter.getJSONArray("aliases")) {
            printOutInformation.append("\t" + alias + "\n");
        }
        // Print out the information
        System.out.println(printOutInformation);

        // if the user chosen y the allegiances will be printed
        //
        // Otherwise the program will skipp this part of the code
        if(option.equals("y")) {
            System.out.println("Allegiances: ");
            // get the allegiances using currentCharacter that holds it
            for(Object ally: currentCharacter.getJSONArray("allegiances")) {
                /*
                 * Getting the allegiances by calling the getSwornMembers function
                */
                for(JSONObject sworn: getSwornMembers(ally.toString())) {
                    System.out.print(("\t" +sworn.getString("name")+ "\n"));
                }
            }
        }
    }
    /*
    * This method will get the sworn member from anapioficeandfire API
    * By receiving the allegiances JSONArray that hold the API request
    * After the information is received the information is put in a ArrayList
    * The ArrayList contains the APT for each character that is a sworn member
    * In Order to receive the name from that character the API will send in getCharacterInformationByHttpRequest()
    */

    public ArrayList<JSONObject> getSwornMembers(String URL) {
        ArrayList<String> gotChars = new ArrayList<>();
        StringBuffer content = null;
        ArrayList<JSONObject> characterJsonObjectArrayList = new ArrayList<>();
        try {
            URL url = new URL(URL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                content = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();
                // Getting the character id and add it to a Arraylist
                JSONObject json = new JSONObject(content.toString());
                for(Object sworn: json.getJSONArray("swornMembers"))  {
                    String swornString = sworn.toString();
                    String characterId = swornString.substring(swornString.lastIndexOf("/")+1);
                    gotChars.add(characterId);
                }
                /*
                 * Loop through the ArrayList in order to pass it on to the getCharacterInformationByHttpRequest
                 * getCharacterInformationByHttpRequest will return a JSON Object for that character
                 * The information is put in a ArrayList to be returned to getCharacter
                 */
                for(String character: gotChars) {
                    characterJsonObjectArrayList.add(getCharacterInformationByHttpRequest(character));
                }
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // return the ArrayList with character information
        return characterJsonObjectArrayList;
    }
    /*
    * This method will get the character information from anapioficeandfire.com
    * The method will return the information as a JSON Object in order to the extracted in getCharacter()
    */
    public JSONObject getCharacterInformationByHttpRequest(String characterIdURL) {
        StringBuffer content = null;
        // URL to the API except the id that will received by characterIdURL
        String URLStringExceptCharacterNumber = "https://anapioficeandfire.com/api/characters/";
        // concatenate the string in order to the entire URL link
        String entireUrlLink = URLStringExceptCharacterNumber + characterIdURL;
        try {
            URL url = new URL(entireUrlLink);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader reader = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(reader);
                String inputLine;
                content = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // return the content as a JSON Object
        return new JSONObject(content.toString());
    }
    /*
    * The method will get books that is published from Bantam Books
    * It will call after books API from anapioficeandfire.com and will loop through the information
    * and will then print it out
    */
    public void getBooks() {
        String bookName= "";
        ArrayList<String> gotBooks = new ArrayList<>();
        ArrayList<JSONObject> gotBook = null;
        StringBuffer content = null;

        String URLString = "https://anapioficeandfire.com/api/books";
        // Getting the information from th API
        try {
            URL url = new URL(URLString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                content = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                // Make the StringBuffer to a JSON Object
                JSONArray books = new JSONArray(content.toString());
                String leftAlignFormat = "| %-20s |%n";
                /*
                * the code snipp will loop through the books and if the publisher is Bantam Books
                * the code will print out the book name and the get the povCharacater that is in that book
                * In format
                *     +----------------------+
                *     |A Dance with Dragons  |
                *     +----------------------+
                *     | Arya Stark           |
                *     | Brandon Stark        |
                *     | Catelyn Stark        |
                *     | Eddard Stark         |
                *     +----------------------+
                */
                for(Object book: books) {

                    if(((JSONObject)book).getString("publisher").equals("Bantam Books")) {

                        bookName = ((JSONObject) book).getString("name");
                        System.out.println();
                        System.out.format("+----------------------+%n");
                        System.out.format("|"     + bookName  +  "     |"+"%n");
                        System.out.format("+----------------------+%n");


                        JSONArray povChar = ((JSONObject) book).getJSONArray("povCharacters");

                        for(Object character: povChar) {

                            String bookCharacter = character.toString();

                            String characterId = bookCharacter .substring(bookCharacter .lastIndexOf("/")+1);

                            gotBooks.add(characterId);

                        }
                        for(String reciveGotBook: gotBooks) {
                            System.out.format(leftAlignFormat,getCharacterInformationByHttpRequest(reciveGotBook).getString("name"));
                        }
                        System.out.format("+----------------------+%n");
                    }
                    gotBooks = new ArrayList<>();
                }
                br.close();
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
