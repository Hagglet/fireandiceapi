Auther Kristoffer Haglund

This program will give to option to search for a GoT character using the API 
from anapioficeandfire.com by id 

The program will then print out basic information about the character 
The user will also be given the option to print out additional information about the sworn member to that character

Lastly the program will print out the povCharacters in the books that has the publisher Bantam Books
the in the format showing below
 +----------------------+
 |A Dance with Dragons  |
 +----------------------+
 | Arya Stark           |
 | Asha Greyjoy         |
 +----------------------+